export class Postal{
    id:number;
    ruta: string;
    nombre: string;
    en_posesion: boolean;
    cant_de_la_postal: number;

    constructor(id:number, nombre:string, ruta:string){
        this.id =id;
        this.nombre = nombre;
        this.ruta = ruta;
        this.en_posesion = false;
        this.cant_de_la_postal = 0;
    }
}