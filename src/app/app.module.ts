import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PagSeleccionComponent } from './pag-seleccion/pag-seleccion.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PagPrincipalComponent } from './pag-principal/pag-principal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PagSeleccionComponent,
    PagPrincipalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
