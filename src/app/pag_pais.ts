import { Postal } from "./postal";

export class Pagina_pais{
    ruta:string;
    nombre:string;
    lista_postales: Postal[];
    porcenteje_lleno: number;
    
    constructor(nombre:string, ruta:string, lista: Postal[]){
        this.nombre = nombre;
        this.ruta = ruta;
        this.lista_postales = lista;   
        this.porcenteje_lleno = 0;  
    }
} 