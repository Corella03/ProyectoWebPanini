import { Pagina_pais } from "./pag_pais";

export class Usuario{
    nombre: string;
    correo: string;
    contra: string;
    cant_total_postales: number;
    cant_postales_unicas: number;
    cant_postales_repetidas: number;
    lista_paises: Pagina_pais[]; 
    constructor(){
        this.cant_total_postales = 0;
        this.cant_postales_unicas = 0;
        this.cant_postales_repetidas = 0;
    }
}