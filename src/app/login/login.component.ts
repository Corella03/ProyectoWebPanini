import { Component, OnInit } from '@angular/core';
//import { ENOMEM } from 'constants';
import { Usuario } from  '../usuario';
import { RouterLink } from '@angular/router';
import swal from 'sweetalert';
import {DatosPaises} from './datos-paises-postales';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public lista_usuarios = [];
  public usuario: Usuario; 
  public existe_usuario: boolean;
  public correo: string;
  public contra:string;
  public paises: DatosPaises;

  constructor() {
    this.usuario = new Usuario();
    this.lista_usuarios = [];
    this.existe_usuario = false;
    this.paises = new DatosPaises;
  }
  ngOnInit() {
    if (JSON.parse(localStorage.getItem("usuario")) == null) {
      this.lista_usuarios = [];
    } else {
      this.lista_usuarios = JSON.parse(localStorage.getItem("usuario"));
    }
  }
  guardar_usuarios(){
    this.paises.llenar_lista_banderas_paises();
    this.usuario.lista_paises = JSON.parse(localStorage.getItem("lista_paises"));
    this.lista_usuarios.push(this.usuario);
    localStorage.setItem( "usuario" , JSON.stringify(this.lista_usuarios));
    this.usuario = new Usuario();
  }
  obtener_usuarios(){
    this.lista_usuarios = JSON.parse(localStorage.getItem("usuario"));
  }
  buscar_usuario_en_lista(){
    for (let i = 0; i < this.lista_usuarios.length; i++) {
      if (this.lista_usuarios[i].correo != this.usuario.correo) {
        console.log("No existe");
        this.existe_usuario = false;
      } else {
        this.existe_usuario = true;
        console.log("Existe");
        return;
      }
    }
  }
  registrar_usuario(): boolean{
    if(this.usuario.nombre != null && this.usuario.correo != null && this.usuario.contra != null){
      if(this.lista_usuarios.length == 0){
        this.guardar_usuarios();
        swal("Registro exitoso!", "", "success");
        return true;
      }else{
      this.buscar_usuario_en_lista();
        if (!this.existe_usuario) {
          this.guardar_usuarios();
          swal("Registro exitoso!", "", "success");
          return true;
        } else {
          swal("Error al registrar!", "El correo ya esta en uso.", "error");
          this.usuario.correo = "";
          return false;
        }
      }
    }
  }
  loguearse(): boolean{
    if(this.correo != null && this.contra != null)
    {
      for (let i = 0; i < this.lista_usuarios.length; i++) {
        if(this.lista_usuarios[i].correo == this.correo && this.lista_usuarios[i].contra == this.contra){
          this.usuario.correo = this.correo;
          //window.location.href = "http://localhost:4200/principal";//solo de la otra forma
          return true;
        }else{
          console.log("login incorrecto");
        }
      }
    }
  }
}
