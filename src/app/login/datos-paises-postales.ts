import {Postal} from '../postal';
import { Pagina_pais } from '../pag_pais';


export class DatosPaises {
    public lista_banderas_paises: Pagina_pais[];
    
    //Grupo A
    public lista_jugadores_russia: Postal[];
    public lista_jugadores_arabia_saudita: Postal[];
    public lista_jugadores_egypto: Postal[];
    public lista_jugadores_uruguay: Postal[];
    //Grupo B
    public lista_jugadores_portugal: Postal[];
    public lista_jugadores_espana: Postal[];
    public lista_jugadores_marruecos: Postal[];
    public lista_jugadores_iran: Postal[];
    //Grupo C
    public lista_jugadores_francia: Postal[];
    public lista_jugadores_australia: Postal[];
    public lista_jugadores_peru: Postal[];
    public lista_jugadores_dinamarca: Postal[];
    //Grupo D
    public lista_jugadores_argentina: Postal[];
    public lista_jugadores_islandia: Postal[];
    public lista_jugadores_croacia: Postal[];
    public lista_jugadores_nigeria: Postal[];
    //Grupo E
    public lista_jugadores_brasil: Postal[];
    public lista_jugadores_suiza: Postal[];
    public lista_jugadores_costa_rica: Postal[];
    public lista_jugadores_serbia: Postal[];
    //Grupo F
    public lista_jugadores_alemania: Postal[];
    public lista_jugadores_mexico: Postal[];
    public lista_jugadores_suecia: Postal[];
    public lista_jugadores_sur_korea: Postal[];
    //Grupo G
    public lista_jugadores_belgica: Postal[];
    public lista_jugadores_panama: Postal[];
    public lista_jugadores_tunes: Postal[];
    public lista_jugadores_inglaterra: Postal[];
    //Grupo H
    public lista_jugadores_polonia: Postal[];
    public lista_jugadores_senegal: Postal[];
    public lista_jugadores_colombia: Postal[];
    public lista_jugadores_japon: Postal[];

    constructor(){
        this.lista_banderas_paises = [];
        this.inicializar_listas();
        this.llamar_metodos();

    }
    inicializar_listas(){
        //Grupo A
        this.lista_jugadores_russia = [];
        this.lista_jugadores_arabia_saudita = [];
        this.lista_jugadores_egypto = [];
        this.lista_jugadores_uruguay = [];
        //Grupo B
        this.lista_jugadores_portugal = [];
        this.lista_jugadores_espana = [];
        this.lista_jugadores_marruecos = [];
        this.lista_jugadores_iran = [];
        //Grupo C
        this.lista_jugadores_francia = [];
        this.lista_jugadores_australia = [];
        this.lista_jugadores_peru = [];
        this.lista_jugadores_dinamarca = [];
        //Grupo D
        this.lista_jugadores_argentina = [];
        this.lista_jugadores_islandia = [];
        this.lista_jugadores_croacia = [];
        this.lista_jugadores_nigeria = [];
        //Grupo E
        this.lista_jugadores_brasil = [];
        this.lista_jugadores_suiza = [];
        this.lista_jugadores_costa_rica = [];
        this.lista_jugadores_serbia = [];
        //Grupo F
        this.lista_jugadores_alemania = [];
        this.lista_jugadores_mexico = [];
        this.lista_jugadores_suecia = [];
        this.lista_jugadores_sur_korea = [];
        //Grupo G
        this.lista_jugadores_belgica = [];
        this.lista_jugadores_panama = [];
        this.lista_jugadores_tunes = [];
        this.lista_jugadores_inglaterra = [];
        //Grupo H
        this.lista_jugadores_polonia = [];
        this.lista_jugadores_senegal = [];
        this.lista_jugadores_colombia = [];
        this.lista_jugadores_japon = [];
    }

    llamar_metodos(){
        //Grupo A
        this.llenar_pais_russia();
        this.llenar_pais_arabia_saudita();
        this.llenar_pais_egypto();
        this.llenar_pais_uruguay();
        //Grubo B
        this.llenar_pais_portugal();
        this.llenar_pais_espana();
        this.llenar_pais_marruecos();
        this.llenar_pais_iran();
        //Grupo C
        this.llenar_pais_francia();
        this.llenar_pais_australia();
        this.llenar_pais_peru();
        this.llenar_pais_dinamarca();
        //Grupo D
        this.llenar_pais_argentina();
        this.llenar_pais_islandia();
        this.llenar_pais_croacia();
        this.llenar_pais_nigeria();
        //Grupo E
        this.llenar_pais_brasil();
        this.llenar_pais_suiza();
        this.llenar_pais_costa_rica();
        this.llenar_pais_serbia();
        //Grupo F
        this.llenar_pais_alemania();
        this.llenar_pais_mexico();
        this.llenar_pais_suecia();
        this.llenar_pais_sur_korea();
        //Grupo G
        this.llenar_pais_belgica();
        this.llenar_pais_panama();
        this.llenar_pais_tunes();
        this.llenar_pais_inglaterra();
        //Grupo H
        this.llenar_pais_polonia();
        this.llenar_pais_senegal();
        this.llenar_pais_colombia();
        this.llenar_pais_japon();
    }
    
    //###############################################  Grupo A ############################################### 

    llenar_pais_russia(){
        this.lista_jugadores_russia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_russia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_russia.push(new Postal(3,"denis_glushakov","denis_glushakov.jpg"));
        this.lista_jugadores_russia.push(new Postal(4,"aleksandr_golovin","aleksandr_golovin.jpg"));
        this.lista_jugadores_russia.push(new Postal(5,"igor_akinfeev","igor_akinfeev.jpg"));
        this.lista_jugadores_russia.push(new Postal(6,"igor_smolnikov","igor_smolnikov.jpg"));
        this.lista_jugadores_russia.push(new Postal(7,"viktor_vasin","viktor_vasin.jpg"));
        this.lista_jugadores_russia.push(new Postal(8,"mario_fernandes","mario_fernandes.jpg"));
        this.lista_jugadores_russia.push(new Postal(9,"yuri_zhirkov","yuri_zhirkov.jpg"));
        this.lista_jugadores_russia.push(new Postal(10,"aleksandr_samedov","aleksandr_samedov.jpg"));
        this.lista_jugadores_russia.push(new Postal(11,"aleksandr_erokhin","aleksandr_erokhin.jpg"));
        this.lista_jugadores_russia.push(new Postal(12,"alan_dzagoev","alan_dzagoev.jpg"));
        this.lista_jugadores_russia.push(new Postal(13,"fedor_kudryashov","fedor_kudryashov.jpg"));
        this.lista_jugadores_russia.push(new Postal(14,"ilya_kutepov","ilya_kutepov.jpg"));
        this.lista_jugadores_russia.push(new Postal(15,"dmitri_kombarov","dmitri_kombarov.jpg"));
        this.lista_jugadores_russia.push(new Postal(16,"aleksei_miranchuk","aleksei_miranchuk.jpg"));
        this.lista_jugadores_russia.push(new Postal(17,"daler_kuzyayev","daler_kuzyayev.jpg"));
        this.lista_jugadores_russia.push(new Postal(18,"dmitry_poloz","dmitry_poloz.png"));
        this.lista_jugadores_russia.push(new Postal(19,"fedor_smolov","fedor_smolov.jpg"));
        this.lista_jugadores_russia.push(new Postal(20,"aleksandr_kokorin","aleksandr_kokorin.jpg"));
        localStorage.setItem( "russia" , JSON.stringify(this.lista_jugadores_russia));
    }
  
    llenar_pais_arabia_saudita(){
        this.lista_jugadores_arabia_saudita.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(3,"taisir_al_jassim","taisir_al_jassim.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(4,"salman_al_faraj","salman_al_faraj.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(5,"abdullah_mayouf","abdullah_mayouf.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(6,"osama_hawsawi","osama_hawsawi.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(7,"abdullah_al_zori","abdullah_al_zori.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(8,"mansour_al_harbi","mansour_al_harbi.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(9,"abdulmalek_al_khaibri","abdulmalek_al_khaibri.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(10,"salman_al_moasher","salman_al_moasher.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(11,"yahya_al_shehri","yahya_al_shehri.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(12,"salem_al_dosari","salem_al_dosari.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(13,"omar_hawsaw","omar_hawsaw.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(14,"yasser_al_shahrani","yasser_al_shahrani.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(15,"motaz_hawsawi","motaz_hawsawi.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(16,"mohammed_al_beraik","mohammed_al_beraik.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(17,"nawaf_al_abed","nawaf_al_abed.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(18,"mohammad_al_sahlawi","mohammad_al_sahlawi.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(19,"nasser_al_shamrani","nasser_al_shamrani.jpg"));
        this.lista_jugadores_arabia_saudita.push(new Postal(20,"fahad_al_muwallad","fahad_al_muwallad.jpg"));
        localStorage.setItem( "arabia_saudita" , JSON.stringify( this.lista_jugadores_arabia_saudita));
    }

    llenar_pais_egypto(){
        this.lista_jugadores_egypto.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_egypto.push(new Postal(2,"equipo","equipo.png"));
        this.lista_jugadores_egypto.push(new Postal(3,"saad_el_din_samir","saad_el_din_samir.jpg"));
        this.lista_jugadores_egypto.push(new Postal(4,"tarek_hamed","tarek_hamed.jpg"));
        this.lista_jugadores_egypto.push(new Postal(5,"essam_el_hadary","essam_el_hadary.jpg"));
        this.lista_jugadores_egypto.push(new Postal(6,"ali_gabr","ali_gabr.jpg"));
        this.lista_jugadores_egypto.push(new Postal(7,"ahmed_elmohamady","ahmed_elmohamady.jpg"));
        this.lista_jugadores_egypto.push(new Postal(8,"omar_gaber","omar_gaber.jpg"));
        this.lista_jugadores_egypto.push(new Postal(9,"mahmoud_kahraba","mahmoud_kahraba.jpg"));
        this.lista_jugadores_egypto.push(new Postal(10,"mohamed_elneny","mohamed_elneny.jpg"));
        this.lista_jugadores_egypto.push(new Postal(11,"trezeguet","trezeguet.jpg"));
        this.lista_jugadores_egypto.push(new Postal(12,"abdallah_el_sai","abdallah_el_sai.jpg"));
        this.lista_jugadores_egypto.push(new Postal(13,"ramy_rabia","ramy_rabia.jpg"));
        this.lista_jugadores_egypto.push(new Postal(14,"ahmed_hegazi","ahmed_hegazi.jpg"));
        this.lista_jugadores_egypto.push(new Postal(15,"ahmed_abdelmonem","ahmed_abdelmonem.jpg"));
        this.lista_jugadores_egypto.push(new Postal(16,"mohamed_abdel_shafy","mohamed_abdel_shafy.jpg"));
        this.lista_jugadores_egypto.push(new Postal(17,"ramadan_sobhi","ramadan_sobhi.jpg"));
        this.lista_jugadores_egypto.push(new Postal(18,"ahmed_hassan","ahmed_hassan.jpg"));
        this.lista_jugadores_egypto.push(new Postal(19,"mohamed_salah","mohamed_salah.png"));
        this.lista_jugadores_egypto.push(new Postal(20,"amr_gamal","amr_gamal.jpg"));
        localStorage.setItem( "egypto" , JSON.stringify( this.lista_jugadores_egypto));
    }

    llenar_pais_uruguay(){
        this.lista_jugadores_uruguay.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(3,"egidio_arevalo_rios","egidio_arevalo_rios.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(4,"alvaro_gonzalez","alvaro_gonzalez.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(5,"fernando_muslera","fernando_muslera.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(6,"maxi_pereira","maxi_pereira.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(7,"diego_godin","diego_godin.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(8,"martin_caceres","martin_caceres.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(9,"nicolas_lodeiro","nicolas_lodeiro.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(10,"carlos_sanchez","carlos_sanchez.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(11,"cristian_rodriguez","cristian_rodriguez.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(12,"matias_vecino","matias_vecino.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(13,"jose_gimenez","jose_gimenez.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(14,"sebastian_coates","sebastian_coates.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(15,"gaston_silva","gaston_silva.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(16,"mathias_corujo","mathias_corujo.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(17,"edinson_cavani","edinson_cavani.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(18,"luis_suarez","luis_suarez.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(19,"cristhian_stuani","cristhian_stuani.jpg"));
        this.lista_jugadores_uruguay.push(new Postal(20,"diego_rolan","diego_rolan.jpg"));
        localStorage.setItem( "uruguay" , JSON.stringify( this.lista_jugadores_uruguay));
    }

 //###############################################  Grupo B ############################################### 


    llenar_pais_portugal(){
        this.lista_jugadores_portugal.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_portugal.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_portugal.push(new Postal(3,"joao_mario","joao_mario.jpg"));
        this.lista_jugadores_portugal.push(new Postal(4,"danilo_pereira","danilo_pereira.jpg"));
        this.lista_jugadores_portugal.push(new Postal(5,"rui_patricio","rui_patricio.jpg"));
        this.lista_jugadores_portugal.push(new Postal(6,"bruno_alves","bruno_alves.jpg"));
        this.lista_jugadores_portugal.push(new Postal(7,"pepe","pepe.jpg"));
        this.lista_jugadores_portugal.push(new Postal(8,"jose_fonte","jose_fonte.jpg"));
        this.lista_jugadores_portugal.push(new Postal(9,"william_carvalho","william_carvalho.jpg"));
        this.lista_jugadores_portugal.push(new Postal(10,"andre_gomes","andre_gomes.jpg"));
        this.lista_jugadores_portugal.push(new Postal(11,"ricardo_quaresma","ricardo_quaresma.jpg"));
        this.lista_jugadores_portugal.push(new Postal(12,"bernardo_silva","bernardo_silva.jpg"));
        this.lista_jugadores_portugal.push(new Postal(13,"eliseu","eliseu.jpg"));
        this.lista_jugadores_portugal.push(new Postal(14,"cedric","cedric.jpg"));
        this.lista_jugadores_portugal.push(new Postal(15,"raphael_guerreiro","raphael_guerreiro.jpg"));
        this.lista_jugadores_portugal.push(new Postal(16,"joao_moutinho","joao_moutinho.jpg"));
        this.lista_jugadores_portugal.push(new Postal(17,"andre_silva","andre_silva.jpg"));
        this.lista_jugadores_portugal.push(new Postal(18,"gelson_martins","gelson_martins.jpg"));
        this.lista_jugadores_portugal.push(new Postal(19,"cristiano_ronaldo","cristiano_ronaldo.jpg"));
        this.lista_jugadores_portugal.push(new Postal(20,"nani","nani.jpg"));
        localStorage.setItem( "portugal" , JSON.stringify( this.lista_jugadores_portugal));
    }


    llenar_pais_espana(){
        this.lista_jugadores_espana.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_espana.push(new Postal(2,"equipo","equipo.png"));
        this.lista_jugadores_espana.push(new Postal(3,"thiago","thiago.png"));
        this.lista_jugadores_espana.push(new Postal(4,"isco","isco.jpg"));
        this.lista_jugadores_espana.push(new Postal(5,"david_de_gea","david_de_gea.jpg"));
        this.lista_jugadores_espana.push(new Postal(6,"jordi_alba","jordi_alba.jpg"));
        this.lista_jugadores_espana.push(new Postal(7,"nacho","nacho.jpg"));
        this.lista_jugadores_espana.push(new Postal(8,"nacho_monreal","nacho_monreal.jpg"));
        this.lista_jugadores_espana.push(new Postal(9,"koke","koke.jpg"));
        this.lista_jugadores_espana.push(new Postal(10,"marco_asensio","marco_asensio.jpg"));
        this.lista_jugadores_espana.push(new Postal(11,"andres_iniesta","andres_iniesta.jpg"));
        this.lista_jugadores_espana.push(new Postal(12,"david_silva","david_silva.jpg"));
        this.lista_jugadores_espana.push(new Postal(13,"gerard_pique","gerard_pique.jpg"));
        this.lista_jugadores_espana.push(new Postal(14,"sergio_ramos","sergio_ramos.jpg"));
        this.lista_jugadores_espana.push(new Postal(15,"daniel_carvajal","daniel_carvajal.jpg"));
        this.lista_jugadores_espana.push(new Postal(16,"sergio_busquets","sergio_busquets.jpg"));
        this.lista_jugadores_espana.push(new Postal(17,"saul_niguez","saul_niguez.jpg"));
        this.lista_jugadores_espana.push(new Postal(18,"alvaro_morata","alvaro_morata.jpg"));
        this.lista_jugadores_espana.push(new Postal(19,"vitolo","vitolo.jpg"));
        this.lista_jugadores_espana.push(new Postal(20,"diego_costa","diego_costa.jpg"));
        localStorage.setItem( "espana" , JSON.stringify( this.lista_jugadores_espana));
    }


    llenar_pais_marruecos(){
        this.lista_jugadores_marruecos.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(3,"karim_el_ahmadi","karim_el_ahmadi.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(4,"younes_belhanda","younes_belhanda.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(5,"munir_mohamedi","munir_mohamedi.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(6,"medhi_benatia","medhi_benatia.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(7,"nabil_dirar","nabil_dirar.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(8,"romain_saiss","romain_saiss.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(9,"nordin_amrabat","nordin_amrabat.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(10,"faycal_fajr","faycal_fajr.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(11,"hakim_ziyech","hakim_ziyech.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(12,"youssef_ait_bennasser","youssef_ait_bennasser.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(13,"fouad_chafik","fouad_chafik.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(14,"hamza_mendyl","hamza_mendyl.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(15,"achraf_hakimi","achraf_hakimi.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(16,"mbark_boussoufa","mbark_boussoufa.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(17,"khalid_boutaib","khalid_boutaib.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(18,"rachid_alioui","rachid_alioui.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(19,"aziz_bouhaddouz","aziz_bouhaddouz.jpg"));
        this.lista_jugadores_marruecos.push(new Postal(20,"youssef_en_nesyri","youssef_en_nesyri.jpg"));
        localStorage.setItem( "marruecos" , JSON.stringify( this.lista_jugadores_marruecos));
    }


    llenar_pais_iran(){
        this.lista_jugadores_iran.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_iran.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_iran.push(new Postal(3,"omid_ebrahimi","omid_ebrahimi.jpg"));
        this.lista_jugadores_iran.push(new Postal(4,"saeid_ezatolahi","saeid_ezatolahi.png"));
        this.lista_jugadores_iran.push(new Postal(5,"alireza_beiranvand","alireza_beiranvand.jpg"));
        this.lista_jugadores_iran.push(new Postal(6,"pejman_montazeri","pejman_montazeri.png"));
        this.lista_jugadores_iran.push(new Postal(7,"vouria_ghafouri","vouria_ghafouri.jpg"));
        this.lista_jugadores_iran.push(new Postal(8,"jalal_hosseini","jalal_hosseini.jpg"));
        this.lista_jugadores_iran.push(new Postal(9,"vahid_amiri","vahid_amiri.jpg"));
        this.lista_jugadores_iran.push(new Postal(10,"alireza_jahanbakhsh","alireza_jahanbakhsh.jpg"));
        this.lista_jugadores_iran.push(new Postal(11,"ashkan_dejagah","ashkan_dejagah.jpg"));
        this.lista_jugadores_iran.push(new Postal(12,"saman_ghoddos","saman_ghoddos.png"));
        this.lista_jugadores_iran.push(new Postal(13,"milad_mohammadi","milad_mohammadi.jpg"));
        this.lista_jugadores_iran.push(new Postal(14,"morteza_pouraliganji","morteza_pouraliganji.jpg"));
        this.lista_jugadores_iran.push(new Postal(15,"ramin_rezaeian","ramin_rezaeian.jpg"));
        this.lista_jugadores_iran.push(new Postal(16,"ehsan_hajsafi","ehsan_hajsafi.jpg"));
        this.lista_jugadores_iran.push(new Postal(17,"mehdi_taremi","mehdi_taremi.jpg"));
        this.lista_jugadores_iran.push(new Postal(18,"karim_ansarifard","karim_ansarifard.jpg"));
        this.lista_jugadores_iran.push(new Postal(19,"reza_ghoochannejhad","reza_ghoochannejhad.png"));
        this.lista_jugadores_iran.push(new Postal(20,"sardar_azmoun","sardar_azmoun.png"));
        localStorage.setItem( "iran" , JSON.stringify( this.lista_jugadores_iran));
    }

 //###############################################  Grupo C ############################################### 

    llenar_pais_francia(){
        this.lista_jugadores_francia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_francia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_francia.push(new Postal(3,"n_golo_kante","n_golo_kante.jpg"));
        this.lista_jugadores_francia.push(new Postal(4,"thomas_lemar","thomas_lemar.jpg"));
        this.lista_jugadores_francia.push(new Postal(5,"hugo_lloris","hugo_lloris.jpg"));
        this.lista_jugadores_francia.push(new Postal(6,"raphael_varane","raphael_varane.jpg"));
        this.lista_jugadores_francia.push(new Postal(7,"lucas_digne","lucas_digne.jpg"));
        this.lista_jugadores_francia.push(new Postal(8,"djibril_sidibe","djibril_sidibe.jpg"));
        this.lista_jugadores_francia.push(new Postal(9,"adrien_rabiot","adrien_rabiot.jpg"));
        this.lista_jugadores_francia.push(new Postal(10,"paul_pogba","paul_pogba.jpg"));
        this.lista_jugadores_francia.push(new Postal(11,"olivier_giroud","olivier_giroud.jpg"));
        this.lista_jugadores_francia.push(new Postal(12,"antoine_griezmann","antoine_griezmann.jpg"));
        this.lista_jugadores_francia.push(new Postal(13,"samuel_umtiti","samuel_umtiti.jpg"));
        this.lista_jugadores_francia.push(new Postal(14,"layvin_kurzawa","layvin_kurzawa.jpg"));
        this.lista_jugadores_francia.push(new Postal(15,"laurent_koscielny","laurent_koscielny.jpg"));
        this.lista_jugadores_francia.push(new Postal(16,"blaise_matuidi","blaise_matuidi.jpg"));
        this.lista_jugadores_francia.push(new Postal(17,"alexandre_lacazette","alexandre_lacazette.jpg"));
        this.lista_jugadores_francia.push(new Postal(18,"kylian_mbappe","kylian_mbappe.jpg"));
        this.lista_jugadores_francia.push(new Postal(19,"ousmane_dembele","ousmane_dembele.jpg"));
        this.lista_jugadores_francia.push(new Postal(20,"anthony_martial","anthony_martial.jpg"));
        localStorage.setItem("francia" , JSON.stringify( this.lista_jugadores_francia));
    }


    llenar_pais_australia(){
        this.lista_jugadores_australia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_australia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_australia.push(new Postal(3,"aaron_mooy","aaron_mooy.jpg"));
        this.lista_jugadores_australia.push(new Postal(4,"james_troisi","james_troisi.jpg"));
        this.lista_jugadores_australia.push(new Postal(5,"mathew_ryan","mathew_ryan.jpg"));
        this.lista_jugadores_australia.push(new Postal(6,"mitchell_langerak","mitchell_langerak.jpg"));
        this.lista_jugadores_australia.push(new Postal(7,"milos_degenek","milos_degenek.jpg"));
        this.lista_jugadores_australia.push(new Postal(8,"bailey_wright","bailey_wright.jpg"));
        this.lista_jugadores_australia.push(new Postal(9,"mile_jedinak","mile_jedinak.jpg"));
        this.lista_jugadores_australia.push(new Postal(10,"massimo_luongo","massimo_luongo.jpg"));
        this.lista_jugadores_australia.push(new Postal(11,"jackson_irvine","jackson_irvine.jpg"));
        this.lista_jugadores_australia.push(new Postal(12,"tom_rogic","tom_rogic.jpg"));
        this.lista_jugadores_australia.push(new Postal(13,"aziz_behich","aziz_behich.jpg"));
        this.lista_jugadores_australia.push(new Postal(14,"trent_sainsbury","trent_sainsbury.jpg"));
        this.lista_jugadores_australia.push(new Postal(15,"ryan_mcgowan","ryan_mcgowan.jpg"));
        this.lista_jugadores_australia.push(new Postal(16,"mark_milligan","mark_milligan.jpg"));
        this.lista_jugadores_australia.push(new Postal(17,"tim_cahill","tim_cahill.jpg"));
        this.lista_jugadores_australia.push(new Postal(18,"mathew_leckie","mathew_leckie.jpg"));
        this.lista_jugadores_australia.push(new Postal(19,"tomi_juric","tomi_juric.jpg"));
        this.lista_jugadores_australia.push(new Postal(20,"robbie_kruse","robbie_kruse.jpg"));
        localStorage.setItem("australia" , JSON.stringify( this.lista_jugadores_australia));
    }


    llenar_pais_peru(){
        this.lista_jugadores_peru.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_peru.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_peru.push(new Postal(3,"christian_cueva","christian_cueva.jpg"));
        this.lista_jugadores_peru.push(new Postal(4,"renato_tapia","renato_tapia.jpg"));
        this.lista_jugadores_peru.push(new Postal(5,"pedro_gallese","pedro_gallese.jpg"));
        this.lista_jugadores_peru.push(new Postal(6,"carlos_caceda","carlos_caceda.jpg"));
        this.lista_jugadores_peru.push(new Postal(7,"aldo_corzo","aldo_corzo.jpg"));
        this.lista_jugadores_peru.push(new Postal(8,"miguel_trauco","miguel_trauco.jpg"));
        this.lista_jugadores_peru.push(new Postal(9,"andy_polo","andy_polo.jpg"));
        this.lista_jugadores_peru.push(new Postal(10,"yoshimar_yotun","yoshimar_yotun.jpg"));
        this.lista_jugadores_peru.push(new Postal(11,"edison_flores","edison_flores.jpg"));
        this.lista_jugadores_peru.push(new Postal(12,"paolo_hurtado","paolo_hurtado.jpg"));
        this.lista_jugadores_peru.push(new Postal(13,"christian_ramos","christian_ramos.jpg"));
        this.lista_jugadores_peru.push(new Postal(14,"luis_advincula","luis_advincula.jpg"));
        this.lista_jugadores_peru.push(new Postal(15,"alberto_rodriguez","alberto_rodriguez.jpg"));
        this.lista_jugadores_peru.push(new Postal(16,"miguel_araujo","miguel_araujo.jpg"));
        this.lista_jugadores_peru.push(new Postal(17,"paolo_guerrero","paolo_guerrero.jpg"));
        this.lista_jugadores_peru.push(new Postal(18,"jefferson_farfan","jefferson_farfan.jpg"));
        this.lista_jugadores_peru.push(new Postal(19,"raul_ruidiaz","raul_ruidiaz.jpg"));
        this.lista_jugadores_peru.push(new Postal(20,"andre_carrillo","andre_carrillo.jpg"));
        localStorage.setItem("peru" , JSON.stringify( this.lista_jugadores_peru));
    }

    llenar_pais_dinamarca(){
        this.lista_jugadores_dinamarca.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(3,"riza_durmisi","riza_durmisi.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(4,"william_kvist","william_kvist.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(5,"kasper_schmeichel","kasper_schmeichel.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(6,"jannik_vestergaard","jannik_vestergaard.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(7,"simon-kjaer_denmark","simon-kjaer_denmark.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(8,"andreas_christensen","andreas_christensen.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(9,"thomas_delaney","thomas_delaney.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(10,"christian_eriksen","christian_eriksen.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(11,"lasse_schone","lasse_schone.png"));
        this.lista_jugadores_dinamarca.push(new Postal(12,"pione_sisto","pione_sisto.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(13,"andreas_bjelland","andreas_bjelland.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(14,"mathias_jorgensen","mathias_jorgensen.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(15,"jens_stryger_larsen","jens_stryger_larsen.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(16,"peter_ankersen","peter_ankersen.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(17,"nicklas_bendtner","nicklas_bendtner.jpg"));
        this.lista_jugadores_dinamarca.push(new Postal(18,"nicolai_jorgensen","nicolai_jorgensen.png"));
        this.lista_jugadores_dinamarca.push(new Postal(19,"yussuf_poulsen","yussuf_poulsen.png"));
        this.lista_jugadores_dinamarca.push(new Postal(20,"andreas_cornelius","andreas_cornelius.jpg"));
        localStorage.setItem("dinamarca" , JSON.stringify( this.lista_jugadores_dinamarca));
    }

     //###############################################  Grupo D ############################################### 

    llenar_pais_argentina(){
        this.lista_jugadores_argentina.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_argentina.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_argentina.push(new Postal(3,"enzo_perez","enzo_perez.png"));
        this.lista_jugadores_argentina.push(new Postal(4,"angel_di_maria","angel_di_maria.png"));
        this.lista_jugadores_argentina.push(new Postal(5,"sergio_romero","sergio_romero.jpg"));
        this.lista_jugadores_argentina.push(new Postal(6,"gabriel_mercado","gabriel_mercado.jpg"));
        this.lista_jugadores_argentina.push(new Postal(7,"federico_fazio","federico_fazio.jpg"));
        this.lista_jugadores_argentina.push(new Postal(8,"javier_mascherano","javier_mascherano.jpg"));
        this.lista_jugadores_argentina.push(new Postal(9,"marcos_acuna","marcos_acuna.jpg"));
        this.lista_jugadores_argentina.push(new Postal(10,"ever_banega","ever_banega.jpg"));
        this.lista_jugadores_argentina.push(new Postal(11,"eduardo_salvio","eduardo_salvio.jpg"));
        this.lista_jugadores_argentina.push(new Postal(12,"mauro_icardi","mauro_icardi.jpg"));
        this.lista_jugadores_argentina.push(new Postal(13,"nicolas_otamendi","nicolas_otamendi.jpg"));
        this.lista_jugadores_argentina.push(new Postal(14,"marcos_rojo","marcos_rojo.jpg"));
        this.lista_jugadores_argentina.push(new Postal(15,"ramiro_funes","ramiro_funes.jpg"));
        this.lista_jugadores_argentina.push(new Postal(16,"lucas_biglia","lucas_biglia.jpg"));
        this.lista_jugadores_argentina.push(new Postal(17,"lionel_messi","lionel_messi.png"));
        this.lista_jugadores_argentina.push(new Postal(18,"paulo_dybala","paulo_dybala.jpg"));
        this.lista_jugadores_argentina.push(new Postal(19,"sergio_aguero","sergio_aguero.png"));
        this.lista_jugadores_argentina.push(new Postal(20,"gonzalo_higuain","gonzalo_higuain.jpg"));

        localStorage.setItem("argentina" , JSON.stringify( this.lista_jugadores_argentina));
    }

    llenar_pais_islandia(){
        this.lista_jugadores_islandia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_islandia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_islandia.push(new Postal(3,"birkir_bjarnason","birkir_bjarnason.png"));
        this.lista_jugadores_islandia.push(new Postal(4,"emil_hallfredsson","emil_hallfredsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(5,"hannes_halldorsson","hannes_halldorsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(6,"birkir_saevarsson","birkir_saevarsson.png"));
        this.lista_jugadores_islandia.push(new Postal(7,"ragnar_sigurosson","ragnar_sigurosson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(8,"kari_arnason","kari_arnason.jpg"));
        this.lista_jugadores_islandia.push(new Postal(9,"gylfi_sigurdsson","gylfi_sigurdsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(10,"arnor_ingvi_traustason","arnor_ingvi_traustason.jpg"));
        this.lista_jugadores_islandia.push(new Postal(11,"rurik_gislason","rurik_gislason.png"));
        this.lista_jugadores_islandia.push(new Postal(12,"johann_gudmundsson","johann_gudmundsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(13,"ari_skulason","ari_skulason.png"));
        this.lista_jugadores_islandia.push(new Postal(14,"sverrir_ingason","sverrir_ingason.png"));
        this.lista_jugadores_islandia.push(new Postal(15,"hordur_magnusson","hordur_magnusson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(16,"aron_gunnarsson","aron_gunnarsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(17,"alfred_finnbogason","alfred_finnbogason.jpg"));
        this.lista_jugadores_islandia.push(new Postal(18,"jon_dadi_bodvarsson","jon_dadi_bodvarsson.jpg"));
        this.lista_jugadores_islandia.push(new Postal(19,"vida_orn_kjartansson","vida_orn_kjartansson.png"));
        this.lista_jugadores_islandia.push(new Postal(20,"bjorn_sigurdarson","bjorn_sigurdarson.jpg"));
        localStorage.setItem("islandia" , JSON.stringify( this.lista_jugadores_islandia));
    }


    llenar_pais_croacia(){
        this.lista_jugadores_croacia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_croacia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_croacia.push(new Postal(3,"luka_modric","luka_modric.jpg"));
        this.lista_jugadores_croacia.push(new Postal(4,"marcelo_brozovic","marcelo_brozovic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(5,"danijel_subasic","danijel_subasic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(6,"sime_vrsaljko","sime_vrsaljko.jpg"));
        this.lista_jugadores_croacia.push(new Postal(7,"ivan_strinic","ivan_strinic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(8,"dejan_lovren","dejan_lovren.png"));
        this.lista_jugadores_croacia.push(new Postal(9,"marko_rog","marko_rog.png"));
        this.lista_jugadores_croacia.push(new Postal(10,"mario _pasalic","mario _pasalic.png"));
        this.lista_jugadores_croacia.push(new Postal(11,"milan_bandelj","milan_bandelj.png"));
        this.lista_jugadores_croacia.push(new Postal(12,"mateo_kovacic","mateo_kovacic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(13,"domagoj_vida","domagoj_vida.jpg"));
        this.lista_jugadores_croacia.push(new Postal(14,"josip_pivaric","josip_pivaric.jpg"));
        this.lista_jugadores_croacia.push(new Postal(15,"vedran_corluka","vedran_corluka.jpg"));
        this.lista_jugadores_croacia.push(new Postal(16,"ivan_rakitic","ivan_rakitic.png"));
        this.lista_jugadores_croacia.push(new Postal(17,"andrej_kramaric","andrej_kramaric.jpg"));
        this.lista_jugadores_croacia.push(new Postal(18,"nikola_kalinic","nikola_kalinic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(19,"mario_mandzukic","mario_mandzukic.jpg"));
        this.lista_jugadores_croacia.push(new Postal(20,"ivan_perisic","ivan_perisic.jpg"));
        localStorage.setItem("croacia" , JSON.stringify( this.lista_jugadores_croacia));
    }


    llenar_pais_nigeria(){
        this.lista_jugadores_nigeria.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(3,"ogenyi_onazi","ogenyi_onazi.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(4,"etebo_oghenekaro","etebo_oghenekaro.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(5,"ikechukwu_ezenwa","ikechukwu_ezenwa.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(6,"elderson_echiejile","elderson_echiejile.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(7,"shehu_abdullahi","shehu_abdullahi.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(8,"william_troost_ekong","william_troost_ekong.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(9,"wilfred_ndidi","wilfred_ndidi.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(10,"mikel_agu","mikel_agu.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(11,"ahmed_musa","ahmed_musa.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(12,"victor_moses","victor_moses.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(13,"leon_balogun","leon_balogun.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(14,"kenneth_omeruo","kenneth_omeruo.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(15,"ola_aina","ola_aina.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(16,"john_obi_mikel","john_obi_mikel.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(17,"moses_simon","moses_simon.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(18,"odion_ighalo","odion_ighalo.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(19,"alex_iwobi","alex_iwobi.jpg"));
        this.lista_jugadores_nigeria.push(new Postal(20,"kelechi_iheanacho","kelechi_iheanacho.jpg"));
        localStorage.setItem("nigeria" , JSON.stringify( this.lista_jugadores_nigeria));
    }

 //###############################################  Grupo E ############################################### 

    llenar_pais_brasil(){
        this.lista_jugadores_brasil.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_brasil.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_brasil.push(new Postal(3,"paulinho","paulinho.jpg"));
        this.lista_jugadores_brasil.push(new Postal(4,"fernandinho","fernandinho.png"));
        this.lista_jugadores_brasil.push(new Postal(5,"alisson","alisson.jpg"));
        this.lista_jugadores_brasil.push(new Postal(6,"dani_alves","dani_alves.jpg"));
        this.lista_jugadores_brasil.push(new Postal(7,"thiago_silva","thiago_silva.jpg"));
        this.lista_jugadores_brasil.push(new Postal(8,"miranda","miranda.jpg"));
        this.lista_jugadores_brasil.push(new Postal(9,"casemiro","casemiro.jpg"));
        this.lista_jugadores_brasil.push(new Postal(10,"renato_augusto","renato_augusto.jpg"));
        this.lista_jugadores_brasil.push(new Postal(11,"giuliano","giuliano.jpg"));
        this.lista_jugadores_brasil.push(new Postal(12,"philippe_coutinho","philippe_coutinho.jpg"));
        this.lista_jugadores_brasil.push(new Postal(13,"filipe_luis","filipe_luis.jpg"));
        this.lista_jugadores_brasil.push(new Postal(14,"marquinhos","marquinhos.jpg"));
        this.lista_jugadores_brasil.push(new Postal(15,"marcelo","marcelo.jpg"));
        this.lista_jugadores_brasil.push(new Postal(16,"willian","willian.jpg"));
        this.lista_jugadores_brasil.push(new Postal(17,"douglas_costa","douglas_costa.jpg"));
        this.lista_jugadores_brasil.push(new Postal(18,"roberto_firmino","roberto_firmino.jpg"));
        this.lista_jugadores_brasil.push(new Postal(19,"gabriel_jesus","gabriel_jesus.jpg"));
        this.lista_jugadores_brasil.push(new Postal(20,"neymar_jr","neymar_jr.jpg"));
        localStorage.setItem("brasil" , JSON.stringify( this.lista_jugadores_brasil));
    }


    llenar_pais_suiza(){
        this.lista_jugadores_suiza.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_suiza.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_suiza.push(new Postal(3,"steven_zuber","steven_zuber.jpg"));
        this.lista_jugadores_suiza.push(new Postal(4,"blerim_dzemaili","blerim_dzemaili.jpg"));
        this.lista_jugadores_suiza.push(new Postal(5,"yann_sommer","yann_sommer.jpg"));
        this.lista_jugadores_suiza.push(new Postal(6,"stephan_lichtsteiner","stephan_lichtsteiner.jpg"));
        this.lista_jugadores_suiza.push(new Postal(7,"manuel_akanji","manuel_akanji.jpg"));
        this.lista_jugadores_suiza.push(new Postal(8,"michael_lang","michael_lang.jpg"));
        this.lista_jugadores_suiza.push(new Postal(9,"denis_zakaria","denis_zakaria.jpg"));
        this.lista_jugadores_suiza.push(new Postal(10,"xherdan_shaqiri","xherdan_shaqiri.jpg"));
        this.lista_jugadores_suiza.push(new Postal(11,"valon_behrami","valon_behrami.jpg"));
        this.lista_jugadores_suiza.push(new Postal(12,"gelson_fernandes","gelson_fernandes.jpg"));
        this.lista_jugadores_suiza.push(new Postal(13,"ricardo_rodriguez","ricardo_rodriguez.jpg"));
        this.lista_jugadores_suiza.push(new Postal(14,"johan_djourou","johan_djourou.jpg"));
        this.lista_jugadores_suiza.push(new Postal(15,"fabian_schar","fabian_schar.jpg"));
        this.lista_jugadores_suiza.push(new Postal(16,"granit_xhaka","granit_xhaka.jpg"));
        this.lista_jugadores_suiza.push(new Postal(17,"breel_embolo","breel_embolo.jpg"));
        this.lista_jugadores_suiza.push(new Postal(18,"haris_seferovic","haris_seferovic.jpg"));
        this.lista_jugadores_suiza.push(new Postal(19,"admir_mehmedi","admir_mehmedi.jpg"));
        this.lista_jugadores_suiza.push(new Postal(20,"eren_derdiyok","eren_derdiyok.jpg"));
        localStorage.setItem("suiza" , JSON.stringify( this.lista_jugadores_suiza));
    }


    llenar_pais_costa_rica(){
        this.lista_jugadores_costa_rica.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(3,"johnny_acosta","johnny_acosta.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(4,"bryan_ruiz","bryan_ruiz.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(5,"keylor_navas","keylor_navas.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(6,"giancarlo_gonzalez","giancarlo_gonzalez.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(7,"cristian_gamboa","cristian_gamboa.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(8,"bryan_oviedo","bryan_oviedo.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(9,"celso_borges","celso_borges.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(10,"christian_bolanos","christian_bolanos.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(11,"randall_azofeifa","randall_azofeifa.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(12,"david_guzman","david_guzman.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(13,"francisco_calvo","francisco_calvo.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(14,"kendall_waston","kendall_waston.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(15,"ronald_matarrita","ronald_matarrita.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(16,"michael_umana","michael_umana.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(17,"rodney_wallace","rodney_wallace.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(18,"joel_campbell","joel_campbell.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(19,"marco_urena","marco_urena.jpg"));
        this.lista_jugadores_costa_rica.push(new Postal(20,"johan_venegas","johan_venegas.jpg"));
        localStorage.setItem("costa_rica" , JSON.stringify( this.lista_jugadores_costa_rica));
        }


    llenar_pais_serbia(){
        this.lista_jugadores_serbia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_serbia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_serbia.push(new Postal(3,"dusan_tadic","dusan_tadic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(4,"sergej_milinkovic_savic","sergej_milinkovic_savic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(5,"vladimir_stojkovic","vladimir_stojkovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(6,"branislav_ivanovic","branislav_ivanovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(7,"aleksandar_kolarov","aleksandar_kolarov.jpg"));
        this.lista_jugadores_serbia.push(new Postal(8,"antonio_rukavina","antonio_rukavina.jpg"));
        this.lista_jugadores_serbia.push(new Postal(9,"nemanja_matic","nemanja_matic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(10,"luka_milivojevic","luka_milivojevic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(11,"adem_ljajic","adem_ljajic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(12,"nemanja_gudelj","nemanja_gudelj.jpg"));
        this.lista_jugadores_serbia.push(new Postal(13,"matija_nastasic","matija_nastasic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(14,"dusko_tosic","dusko_tosic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(15,"nikola_maksimovic","nikola_maksimovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(16,"jagos_vukovic","jagos_vukovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(17,"mijat_gacinovic","mijat_gacinovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(18,"filip_kostic","filip_kostic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(19,"aleksandar_mitrovic","aleksandar_mitrovic.jpg"));
        this.lista_jugadores_serbia.push(new Postal(20,"aleksandar_prijovic","aleksandar_prijovic.jpg"));
        localStorage.setItem("serbia" , JSON.stringify( this.lista_jugadores_serbia));
    }

 //###############################################  Grupo F ############################################### 

    llenar_pais_alemania(){
        this.lista_jugadores_alemania.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_alemania.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_alemania.push(new Postal(3,"emre_can","emre_can.jpg"));
        this.lista_jugadores_alemania.push(new Postal(4,"leon_goretzka","leon_goretzka.jpg"));
        this.lista_jugadores_alemania.push(new Postal(5,"manuel_neuer","manuel_neuer.jpg"));
        this.lista_jugadores_alemania.push(new Postal(6,"mats_hummels","mats_hummels.jpg"));
        this.lista_jugadores_alemania.push(new Postal(7,"antonio_rudiger","antonio_rudiger.jpg"));
        this.lista_jugadores_alemania.push(new Postal(8,"jerome_boateng","jerome_boateng.jpg"));
        this.lista_jugadores_alemania.push(new Postal(9,"julian_brandt","julian_brandt.jpg"));
        this.lista_jugadores_alemania.push(new Postal(10,"sebastian_rudy","sebastian_rudy.jpg"));
        this.lista_jugadores_alemania.push(new Postal(11,"leroy_sane","leroy_sane.jpg"));
        this.lista_jugadores_alemania.push(new Postal(12,"mesut_ozil","mesut_ozil.jpg"));
        this.lista_jugadores_alemania.push(new Postal(13,"joshua_kimmich","joshua_kimmich.jpg"));
        this.lista_jugadores_alemania.push(new Postal(14,"jonas_hector","jonas_hector.jpg"));
        this.lista_jugadores_alemania.push(new Postal(15,"julian_draxler","julian_draxler.jpg"));
        this.lista_jugadores_alemania.push(new Postal(16,"toni_kroos","toni_kroos.jpg"));
        this.lista_jugadores_alemania.push(new Postal(17,"sami_khedira","sami_khedira.jpg"));
        this.lista_jugadores_alemania.push(new Postal(18,"mario_gotze","mario_gotze.jpg"));
        this.lista_jugadores_alemania.push(new Postal(19,"thomas_muller","thomas_muller.jpg"));
        this.lista_jugadores_alemania.push(new Postal(20,"timo_werner","timo_werner.jpg"));
        localStorage.setItem("alemania" , JSON.stringify( this.lista_jugadores_alemania));
    }


    llenar_pais_mexico(){
        this.lista_jugadores_mexico.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_mexico.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_mexico.push(new Postal(3,"giovani_dos_santos","giovani_dos_santos.jpg"));
        this.lista_jugadores_mexico.push(new Postal(4,"andres_guardado","andres_guardado.jpg"));
        this.lista_jugadores_mexico.push(new Postal(5,"guillermo_ochoa","guillermo_ochoa.jpg"));
        this.lista_jugadores_mexico.push(new Postal(6,"hugo_ayala","hugo_ayala.jpg"));
        this.lista_jugadores_mexico.push(new Postal(7,"diego_reyes","diego_reyes.jpg"));
        this.lista_jugadores_mexico.push(new Postal(8,"hector_moreno","hector_moreno.jpg"));
        this.lista_jugadores_mexico.push(new Postal(9,"hector_herrera","hector_herrera.jpg"));
        this.lista_jugadores_mexico.push(new Postal(10,"javier_aquino","javier_aquino.jpg"));
        this.lista_jugadores_mexico.push(new Postal(11,"jesus_corona","jesus_corona.jpg"));
        this.lista_jugadores_mexico.push(new Postal(12,"hirving_lozano","hirving_lozano.jpg"));
        this.lista_jugadores_mexico.push(new Postal(13,"jesus_gallardo","jesus_gallardo.jpg"));
        this.lista_jugadores_mexico.push(new Postal(14,"miguel_layun","miguel_layun.jpg"));
        this.lista_jugadores_mexico.push(new Postal(15,"carlos_salcedo","carlos_salcedo.jpg"));
        this.lista_jugadores_mexico.push(new Postal(16,"jonathan_dos_santos","jonathan_dos_santos.jpg"));
        this.lista_jugadores_mexico.push(new Postal(17,"raul_jimenez","raul_jimenez.jpg"));
        this.lista_jugadores_mexico.push(new Postal(18,"carlos_vela","carlos_vela.jpg"));
        this.lista_jugadores_mexico.push(new Postal(19,"javier_hernandez","javier_hernandez.jpg"));
        this.lista_jugadores_mexico.push(new Postal(20,"oribe_peralta","oribe_peralta.jpg"));
        localStorage.setItem("mexico" , JSON.stringify( this.lista_jugadores_mexico));
    }



    llenar_pais_suecia(){
        this.lista_jugadores_suecia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_suecia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_suecia.push(new Postal(3,"sebastian_larsson","sebastian_larsson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(4,"emil_forsberg","emil_forsberg.jpg"));
        this.lista_jugadores_suecia.push(new Postal(5,"robin_olsen","robin_olsen.jpg"));
        this.lista_jugadores_suecia.push(new Postal(6,"mikael_lustig","mikael_lustig.jpg"));
        this.lista_jugadores_suecia.push(new Postal(7,"victor_nilsson_lindelof","victor_nilsson_lindelof.jpg"));
        this.lista_jugadores_suecia.push(new Postal(8,"andreas_granqvist","andreas_granqvist.jpg"));
        this.lista_jugadores_suecia.push(new Postal(9,"gustav_svensson","gustav_svensson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(10,"viktor_claesson","viktor_claesson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(11,"jimmy_durmaz","jimmy_durmaz.jpg"));
        this.lista_jugadores_suecia.push(new Postal(12,"albin_ekdal","albin_ekdal.jpg"));
        this.lista_jugadores_suecia.push(new Postal(13,"martin_olsson","martin_olsson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(14,"ludwig_augustinsson","ludwig_augustinsson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(15,"emil_krafth","emil_krafth.jpg"));
        this.lista_jugadores_suecia.push(new Postal(16,"pontus_jansson","pontus_jansson.jpg"));
        this.lista_jugadores_suecia.push(new Postal(17,"isaac_kiese_thelin","isaac_kiese_thelin.jpg"));
        this.lista_jugadores_suecia.push(new Postal(18,"marcus_berg","marcus_berg.jpg"));
        this.lista_jugadores_suecia.push(new Postal(19,"john_guidetti","john_guidetti.jpg"));
        this.lista_jugadores_suecia.push(new Postal(20,"ola_toivonen","ola_toivonen.jpg"));
        localStorage.setItem("suecia" , JSON.stringify( this.lista_jugadores_suecia));
    }


    llenar_pais_sur_korea(){
        this.lista_jugadores_sur_korea.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(3,"ki_sungyueng","ki_sungyueng.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(4,"nam_taehee","nam_taehee.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(5,"kim_seunggyu","kim_seunggyu.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(6,"kim_younggwon","kim_younggwon.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(7,"kim_jinsu","kim_jinsu.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(8,"kwak_taehwi","kwak_taehwi.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(9,"han_kookyoung","han_kookyoung.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(10,"lee_chungyong","lee_chungyong.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(11,"jung_wooyoung","jung_wooyoung.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(12,"lee_jaesung","lee_jaesung.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(13,"hong_jeongho","hong_jeongho.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(14,"jang_hyunsoo","jang_hyunsoo.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(15,"koo_jacheol","koo_jacheol.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(16,"kwon_changhoon","kwon_changhoon.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(17,"son_heungmin","son_heungmin.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(18,"ji_dongwon","ji_dongwon.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(19,"kim_shinwook","kim_shinwook.jpg"));
        this.lista_jugadores_sur_korea.push(new Postal(20,"hwang_heechan","hwang_heechan.jpg"));
        localStorage.setItem("sur_korea" , JSON.stringify( this.lista_jugadores_sur_korea));
    }

 //###############################################  Grupo G ############################################### 

    llenar_pais_belgica(){
        this.lista_jugadores_belgica.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_belgica.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_belgica.push(new Postal(3,"kevin_de_bruyne","kevin_de_bruyne.jpg"));
        this.lista_jugadores_belgica.push(new Postal(4,"maroune_fellaini","maroune_fellaini.png"));
        this.lista_jugadores_belgica.push(new Postal(5,"thibaut_courtois","thibaut_courtois.jpg"));
        this.lista_jugadores_belgica.push(new Postal(6,"toby_alderweireld","toby_alderweireld.jpg"));
        this.lista_jugadores_belgica.push(new Postal(7,"thomas_vermaelen","thomas_vermaelen.jpg"));
        this.lista_jugadores_belgica.push(new Postal(8,"jan_vertonghen","jan_vertonghen.jpg"));
        this.lista_jugadores_belgica.push(new Postal(9,"youri_tielemans","youri_tielemans.jpg"));
        this.lista_jugadores_belgica.push(new Postal(10,"mousa_dembele","mousa_dembele.jpg"));
        this.lista_jugadores_belgica.push(new Postal(11,"nacer_chadli","nacer_chadli.jpg"));
        this.lista_jugadores_belgica.push(new Postal(12,"eden_hazard","eden_hazard.jpg"));
        this.lista_jugadores_belgica.push(new Postal(13,"vincent_kompany","vincent_kompany.png"));
        this.lista_jugadores_belgica.push(new Postal(14,"thomas_meunier","thomas_meunier.png"));
        this.lista_jugadores_belgica.push(new Postal(15,"axel_witse","axel_witse.jpg"));
        this.lista_jugadores_belgica.push(new Postal(16,"radja_nainggolan","radja_nainggolan.jpg"));
        this.lista_jugadores_belgica.push(new Postal(17,"yannick_carrasco","yannick_carrasco.jpg"));
        this.lista_jugadores_belgica.push(new Postal(18,"dries_mertens","dries_mertens.jpg"));
        this.lista_jugadores_belgica.push(new Postal(19,"michy_batshuayi","michy_batshuayi.png"));
        this.lista_jugadores_belgica.push(new Postal(20,"romelu_lukaku","romelu_lukaku.jpg"));
        localStorage.setItem("belgica" , JSON.stringify( this.lista_jugadores_belgica));
    }


    llenar_pais_panama(){
        this.lista_jugadores_panama.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_panama.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_panama.push(new Postal(3,"felipe_baloy","felipe_baloy.jpg"));
        this.lista_jugadores_panama.push(new Postal(4,"gabriel_gomez","gabriel_gomez.jpg"));
        this.lista_jugadores_panama.push(new Postal(5,"jaime_penedo","jaime_penedo.jpg"));
        this.lista_jugadores_panama.push(new Postal(6,"jose_calderon","jose_calderon.jpg"));
        this.lista_jugadores_panama.push(new Postal(7,"michael_murillo","michael_murillo.jpg"));
        this.lista_jugadores_panama.push(new Postal(8,"fidel_escobar","fidel_escobar.jpg"));
        this.lista_jugadores_panama.push(new Postal(9,"edgar_barcenas","edgar_barcenas.jpg"));
        this.lista_jugadores_panama.push(new Postal(10,"armando","armando.jpg"));
        this.lista_jugadores_panama.push(new Postal(11,"alberto_quintero","alberto_quintero.jpg"));
        this.lista_jugadores_panama.push(new Postal(12,"anibal_godoy","anibal_godoy.jpg"));
        this.lista_jugadores_panama.push(new Postal(13,"roman_torres","roman_torres.jpg"));
        this.lista_jugadores_panama.push(new Postal(14,"adolfo_machado","adolfo_machado.jpg"));
        this.lista_jugadores_panama.push(new Postal(15,"eric_davis","eric_davis.jpg"));
        this.lista_jugadores_panama.push(new Postal(16,"luis_ovalle","luis_ovalle.jpg"));
        this.lista_jugadores_panama.push(new Postal(17,"blas_perez","blas_perez.jpg"));
        this.lista_jugadores_panama.push(new Postal(18,"gabriel_torres","gabriel_torres.jpg"));
        this.lista_jugadores_panama.push(new Postal(19,"luis_tejada","luis_tejada.jpg"));
        this.lista_jugadores_panama.push(new Postal(20,"abdiel_arroyo","abdiel_arroyo.jpg"));
        localStorage.setItem("panama" , JSON.stringify( this.lista_jugadores_panama));
    }


    llenar_pais_tunes(){
        this.lista_jugadores_tunes.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_tunes.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_tunes.push(new Postal(3,"wahbi_khazri","wahbi_khazri.jpg"));
        this.lista_jugadores_tunes.push(new Postal(4,"mohamed_amine_ben_amor","mohamed_amine_ben_amor.jpg"));
        this.lista_jugadores_tunes.push(new Postal(5,"aymen_mathlouthi","aymen_mathlouthi.jpg"));
        this.lista_jugadores_tunes.push(new Postal(6,"ali_maaloul","ali_maaloul.jpg"));
        this.lista_jugadores_tunes.push(new Postal(7,"syam_ben_youssef","syam_ben_youssef.jpg"));
        this.lista_jugadores_tunes.push(new Postal(8,"aymen_abdennour","aymen_abdennour.jpg"));
        this.lista_jugadores_tunes.push(new Postal(9,"ghailene_chaalali","ghailene_chaalali.jpg"));
        this.lista_jugadores_tunes.push(new Postal(10,"naim_sliti","naim_sliti.jpg"));
        this.lista_jugadores_tunes.push(new Postal(11,"youssef_msakni","youssef_msakni.jpg"));
        this.lista_jugadores_tunes.push(new Postal(12,"fakhreddine_ben_youssef","fakhreddine_ben_youssef.jpg"));
        this.lista_jugadores_tunes.push(new Postal(13,"hamdi_nagguez","hamdi_nagguez.jpg"));
        this.lista_jugadores_tunes.push(new Postal(14,"yassine_meriah","yassine_meriah.jpg"));
        this.lista_jugadores_tunes.push(new Postal(15,"oussama_haddadi","oussama_haddadi.jpg"));
        this.lista_jugadores_tunes.push(new Postal(16,"ferjani_sassi","ferjani_sassi.jpg"));
        this.lista_jugadores_tunes.push(new Postal(17,"taha_yassine_khenissi","taha_yassine_khenissi.jpg"));
        this.lista_jugadores_tunes.push(new Postal(18,"yoann_touzghar","yoann_touzghar.jpg"));
        this.lista_jugadores_tunes.push(new Postal(19,"anice_badri","anice_badri.jpg"));
        this.lista_jugadores_tunes.push(new Postal(20,"ahmed_akaichi","ahmed_akaichi.jpg"));
        localStorage.setItem("tunes" , JSON.stringify( this.lista_jugadores_tunes));
    }

    llenar_pais_inglaterra(){
        this.lista_jugadores_inglaterra.push(new Postal(1,"emblema","emblema.png"));
        this.lista_jugadores_inglaterra.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(3,"jordan_henderson","jordan_henderson.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(4,"alex_oxlade_chamberlain","alex_oxlade_chamberlain.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(5,"joe_hart","joe_hart.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(6,"jordan_pickford","jordan_pickford.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(7,"gary_cahill","gary_cahill.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(8,"kyle_walker","kyle_walker.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(9,"dele_alli","dele_alli.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(10,"eric_dier","eric_dier.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(11,"adam_lallana","adam_lallana.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(12,"jesse_lingard","jesse_lingard.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(13,"john_stones","john_stones.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(14,"ryan_bertrand","ryan_bertrand.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(15,"danny_rose","danny_rose.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(16,"phil_jones","phil_jones.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(17,"raheem_sterling","raheem_sterling.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(18,"harry_kane","harry_kane.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(19,"marcus_rashford","marcus_rashford.jpg"));
        this.lista_jugadores_inglaterra.push(new Postal(20,"jamie_vardy","jamie_vardy.jpg"));
        localStorage.setItem("inglaterra" , JSON.stringify( this.lista_jugadores_inglaterra));
    }

 //###############################################  Grupo H ############################################### 

    llenar_pais_polonia(){
        this.lista_jugadores_polonia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_polonia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_polonia.push(new Postal(3,"maciej_rybus","maciej_rybus.jpg"));
        this.lista_jugadores_polonia.push(new Postal(4,"jakub_blaszczykowski","jakub_blaszczykowski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(5,"lukasz_fabianski","lukasz_fabianski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(6,"wojciech_szczesny","wojciech_szczesny.jpg"));
        this.lista_jugadores_polonia.push(new Postal(7,"lukasz_piszczek","lukasz_piszczek.jpg"));
        this.lista_jugadores_polonia.push(new Postal(8,"kamil_glik","kamil_glik.jpg"));
        this.lista_jugadores_polonia.push(new Postal(9,"kamil_grosicki","kamil_grosicki.jpg"));
        this.lista_jugadores_polonia.push(new Postal(10,"grzegorz_krychowiak","grzegorz_krychowiak.jpg"));
        this.lista_jugadores_polonia.push(new Postal(11,"krzysztof_maczynski","krzysztof_maczynski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(12,"piotr_zielinski","piotr_zielinski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(13,"michal_pazdan","michal_pazdan.jpg"));
        this.lista_jugadores_polonia.push(new Postal(14,"thiago_cionek","thiago_cionek.jpg"));
        this.lista_jugadores_polonia.push(new Postal(15,"bartosz_bereszynski","bartosz_bereszynski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(16,"artur_jedrzejczyk","artur_jedrzejczyk.jpg"));
        this.lista_jugadores_polonia.push(new Postal(17,"karol_linetty","karol_linetty.jpg"));
        this.lista_jugadores_polonia.push(new Postal(18,"robert_lewandowski","robert_lewandowski.jpg"));
        this.lista_jugadores_polonia.push(new Postal(19,"lukasz_teodorczyk","lukasz_teodorczyk.jpg"));
        this.lista_jugadores_polonia.push(new Postal(20,"arkadiusz_milik","arkadiusz_milik.jpg"));
        localStorage.setItem("polonia" , JSON.stringify( this.lista_jugadores_polonia));
    }


    llenar_pais_senegal(){
        this.lista_jugadores_senegal.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_senegal.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_senegal.push(new Postal(3,"cheikhou_kouyate","cheikhou_kouyate.jpg"));
        this.lista_jugadores_senegal.push(new Postal(4,"cheikh_ndoye","cheikh_ndoye.jpg"));
        this.lista_jugadores_senegal.push(new Postal(5,"khadim_n_diaye","khadim_n_diaye.jpg"));
        this.lista_jugadores_senegal.push(new Postal(6,"kara_mbodji","kara_mbodji.jpg"));
        this.lista_jugadores_senegal.push(new Postal(7,"lamine_gassama","lamine_gassama.jpg"));
        this.lista_jugadores_senegal.push(new Postal(8,"kalidou_koulibaly","kalidou_koulibaly.jpg"));
        this.lista_jugadores_senegal.push(new Postal(9,"pape_alioune_n_diaye","pape_alioune_n_diaye.jpg"));
        this.lista_jugadores_senegal.push(new Postal(10,"sadio_mane","sadio_mane.jpg"));
        this.lista_jugadores_senegal.push(new Postal(11,"moussa_sow","moussa_sow.jpg"));
        this.lista_jugadores_senegal.push(new Postal(12,"moussa_konate","moussa_konate.jpg"));
        this.lista_jugadores_senegal.push(new Postal(13,"salif_sane","salif_sane.jpg"));
        this.lista_jugadores_senegal.push(new Postal(14,"saliou_ciss","saliou_ciss.jpg"));
        this.lista_jugadores_senegal.push(new Postal(15,"moussa_wague","moussa_wague.jpg"));
        this.lista_jugadores_senegal.push(new Postal(16,"idrissa_gueye","idrissa_gueye.jpg"));
        this.lista_jugadores_senegal.push(new Postal(17,"keita_balde","keita_balde.jpg"));
        this.lista_jugadores_senegal.push(new Postal(18,"m_baye_niang","m_baye_niang.jpg"));
        this.lista_jugadores_senegal.push(new Postal(19,"ismaila_sarr","ismaila_sarr.jpg"));
        this.lista_jugadores_senegal.push(new Postal(20,"mame_diouf","mame_diouf.jpg"));
        localStorage.setItem("senegal" , JSON.stringify( this.lista_jugadores_senegal));
    }


    llenar_pais_colombia(){
        this.lista_jugadores_colombia.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_colombia.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_colombia.push(new Postal(3,"abel_aguilar","abel_aguilar.jpg"));
        this.lista_jugadores_colombia.push(new Postal(4,"james_rodriguez","james_rodriguez.jpg"));
        this.lista_jugadores_colombia.push(new Postal(5,"david_ospina","david_ospina.jpg"));
        this.lista_jugadores_colombia.push(new Postal(6,"cristian_zapata","cristian_zapata.jpg"));
        this.lista_jugadores_colombia.push(new Postal(7,"santiago_arias","santiago_arias.jpg"));
        this.lista_jugadores_colombia.push(new Postal(8,"frank_fabra","frank_fabra.jpg"));
        this.lista_jugadores_colombia.push(new Postal(9,"giovanni_moreno","giovanni_moreno.jpg"));
        this.lista_jugadores_colombia.push(new Postal(10,"wilmar_barrios","wilmar_barrios.jpg"));
        this.lista_jugadores_colombia.push(new Postal(11,"radamel_falcao_garcia","radamel_falcao_garcia.jpg"));
        this.lista_jugadores_colombia.push(new Postal(12,"teofilo_gutierrez","teofilo_gutierrez.jpg"));
        this.lista_jugadores_colombia.push(new Postal(13,"davinson_sanchez","davinson_sanchez.jpg"));
        this.lista_jugadores_colombia.push(new Postal(14,"yerry_mina","yerry_mina.jpg"));
        this.lista_jugadores_colombia.push(new Postal(15,"carlos_sanchez","carlos_sanchez.jpg"));
        this.lista_jugadores_colombia.push(new Postal(16,"juan_guillermo_cuadrado","juan_guillermo_cuadrado.jpg"));
        this.lista_jugadores_colombia.push(new Postal(17,"carlos_bacca","carlos_bacca.jpg"));
        this.lista_jugadores_colombia.push(new Postal(18,"edwin_cardona","edwin_cardona.jpg"));
        this.lista_jugadores_colombia.push(new Postal(19,"yimmi_chara","yimmi_chara.jpg"));
        this.lista_jugadores_colombia.push(new Postal(20,"duvan_zapata","duvan_zapata.jpg"));
        localStorage.setItem("colombia" , JSON.stringify( this.lista_jugadores_colombia));
    }


    llenar_pais_japon(){
        this.lista_jugadores_japon.push(new Postal(1,"emblema","emblema.jpg"));
        this.lista_jugadores_japon.push(new Postal(2,"equipo","equipo.jpg"));
        this.lista_jugadores_japon.push(new Postal(3,"hotaru_yamaguchi","hotaru_yamaguchi.jpg"));
        this.lista_jugadores_japon.push(new Postal(4,"shinji_kagawa","shinji_kagawa.jpg"));
        this.lista_jugadores_japon.push(new Postal(5,"eiji_kawashima","eiji_kawashima.jpg"));
        this.lista_jugadores_japon.push(new Postal(6,"shusaku_nishikawa","shusaku_nishikawa.jpg"));
        this.lista_jugadores_japon.push(new Postal(7,"masato_morishige","masato_morishige.png"));
        this.lista_jugadores_japon.push(new Postal(8,"yuto_nagatomo","yuto_nagatomo.jpg"));
        this.lista_jugadores_japon.push(new Postal(9,"makoto_hasebe","makoto_hasebe.jpg"));
        this.lista_jugadores_japon.push(new Postal(10,"hiroshi_kiyotake","hiroshi_kiyotake.jpg"));
        this.lista_jugadores_japon.push(new Postal(11,"keisuke_honda","keisuke_honda.jpg"));
        this.lista_jugadores_japon.push(new Postal(12,"takashi_usami","takashi_usami.jpg"));
        this.lista_jugadores_japon.push(new Postal(13,"hiroki_sakai","hiroki_sakai.jpg"));
        this.lista_jugadores_japon.push(new Postal(14,"tomoaki_makino","tomoaki_makino.jpg"));
        this.lista_jugadores_japon.push(new Postal(15,"gotoku_sakai","gotoku_sakai.jpg"));
        this.lista_jugadores_japon.push(new Postal(16,"maya_yoshida","maya_yoshida.jpg"));
        this.lista_jugadores_japon.push(new Postal(17,"genki_haraguchi","genki_haraguchi.jpg"));
        this.lista_jugadores_japon.push(new Postal(18,"shinji_okazaki","shinji_okazaki.jpg"));
        this.lista_jugadores_japon.push(new Postal(19,"yuya-kubo","yuya-kubo.jpg"));
        this.lista_jugadores_japon.push(new Postal(20,"yuya_osako","yuya_osako.jpg"));
        localStorage.setItem("japon" , JSON.stringify( this.lista_jugadores_japon));
    }

 //###############################################  Se insertan los jugadores a cada pais ############################################### 

    llenar_lista_banderas_paises() {
        //Grupo A
        this.lista_banderas_paises.push(new Pagina_pais("russia","assets/paises/russia.jpg",  JSON.parse(localStorage.getItem("russia"))));
        this.lista_banderas_paises.push(new Pagina_pais("arabia_saudita","assets/paises/arabia_saudita.jpg", JSON.parse(localStorage.getItem("arabia_saudita"))));
        this.lista_banderas_paises.push(new Pagina_pais("egypto","assets/paises/egypto.jpg", JSON.parse(localStorage.getItem("egypto"))));
        this.lista_banderas_paises.push(new Pagina_pais("uruguay","assets/paises/uruguay.jpg", JSON.parse(localStorage.getItem("uruguay"))));
        //Grupo B
        this.lista_banderas_paises.push(new Pagina_pais("portugal","assets/paises/portugal.jpg", JSON.parse(localStorage.getItem("portugal"))));
        this.lista_banderas_paises.push(new Pagina_pais("espana","assets/paises/espana.jpg", JSON.parse(localStorage.getItem("espana"))));
        this.lista_banderas_paises.push(new Pagina_pais("marruecos","assets/paises/marruecos.jpg", JSON.parse(localStorage.getItem("marruecos"))));
        this.lista_banderas_paises.push(new Pagina_pais("iran","assets/paises/iran.jpg", JSON.parse(localStorage.getItem("iran"))));
        //Grupo C
        this.lista_banderas_paises.push(new Pagina_pais("francia","assets/paises/francia.jpg", JSON.parse(localStorage.getItem("francia"))));
        this.lista_banderas_paises.push(new Pagina_pais("australia","assets/paises/australia.jpg", JSON.parse(localStorage.getItem("australia"))));
        this.lista_banderas_paises.push(new Pagina_pais("peru","assets/paises/peru.jpg", JSON.parse(localStorage.getItem("peru"))));
        this.lista_banderas_paises.push(new Pagina_pais("dinamarca","assets/paises/dinamarca.jpg", JSON.parse(localStorage.getItem("dinamarca"))));
        //Grupo D
        this.lista_banderas_paises.push(new Pagina_pais("argentina","assets/paises/argentina.jpg", JSON.parse(localStorage.getItem("argentina"))));
        this.lista_banderas_paises.push(new Pagina_pais("islandia","assets/paises/islandia.jpg", JSON.parse(localStorage.getItem("islandia"))));
        this.lista_banderas_paises.push(new Pagina_pais("croacia","assets/paises/croacia.jpg", JSON.parse(localStorage.getItem("croacia"))));
        this.lista_banderas_paises.push(new Pagina_pais("nigeria","assets/paises/nigeria.jpg", JSON.parse(localStorage.getItem("nigeria"))));
        //Grupo E
        this.lista_banderas_paises.push(new Pagina_pais("brasil","assets/paises/brasil.jpg", JSON.parse(localStorage.getItem("brasil"))));
        this.lista_banderas_paises.push(new Pagina_pais("suiza","assets/paises/suiza.jpg", JSON.parse(localStorage.getItem("suiza"))));
        this.lista_banderas_paises.push(new Pagina_pais("costa_rica","assets/paises/costa_rica.jpg", JSON.parse(localStorage.getItem("costa_rica"))));
        this.lista_banderas_paises.push(new Pagina_pais("serbia","assets/paises/serbia.jpg", JSON.parse(localStorage.getItem("serbia"))));
        //Grupo F
        this.lista_banderas_paises.push(new Pagina_pais("alemania","assets/paises/alemania.jpg", JSON.parse(localStorage.getItem("alemania"))));
        this.lista_banderas_paises.push(new Pagina_pais("mexico","assets/paises/mexico.jpg", JSON.parse(localStorage.getItem("mexico"))));
        this.lista_banderas_paises.push(new Pagina_pais("suecia","assets/paises/suecia.jpg", JSON.parse(localStorage.getItem("suecia"))));
        this.lista_banderas_paises.push(new Pagina_pais("sur_korea","assets/paises/sur_korea.jpg", JSON.parse(localStorage.getItem("sur_korea"))));
        //Grupo G
        this.lista_banderas_paises.push(new Pagina_pais("belgica","assets/paises/belgica.jpg", JSON.parse(localStorage.getItem("belgica"))));
        this.lista_banderas_paises.push(new Pagina_pais("panama","assets/paises/panama.jpg", JSON.parse(localStorage.getItem("panama"))));
        this.lista_banderas_paises.push(new Pagina_pais("tunes","assets/paises/tunes.jpg", JSON.parse(localStorage.getItem("tunes"))));
        this.lista_banderas_paises.push(new Pagina_pais("inglaterra","assets/paises/inglaterra.jpg", JSON.parse(localStorage.getItem("inglaterra"))));
        //Grupo  H
        this.lista_banderas_paises.push(new Pagina_pais("polonia","assets/paises/polonia.jpg", JSON.parse(localStorage.getItem("polonia"))));
        this.lista_banderas_paises.push(new Pagina_pais("senegal","assets/paises/senegal.jpg", JSON.parse(localStorage.getItem("senegal"))));
        this.lista_banderas_paises.push(new Pagina_pais("colombia","assets/paises/colombia.jpg", JSON.parse(localStorage.getItem("colombia"))));
        this.lista_banderas_paises.push(new Pagina_pais("japon","assets/paises/japon.jpg", JSON.parse(localStorage.getItem("japon"))));
        localStorage.setItem("lista_paises" , JSON.stringify( this.lista_banderas_paises));
      }
}
