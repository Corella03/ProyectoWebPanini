import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


import { LoginComponent } from '../login/login.component';
import { PagSeleccionComponent } from '../pag-seleccion/pag-seleccion.component';
import { PagPrincipalComponent } from '../pag-principal/pag-principal.component';

const routes: Routes = [
	{ path: '', component: LoginComponent },
  { path: 'pag/:correo', component: PagSeleccionComponent },
  { path: 'principal/:correo', component: PagPrincipalComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }







