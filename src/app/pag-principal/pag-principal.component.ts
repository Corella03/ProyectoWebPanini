import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-pag-principal',
  templateUrl: './pag-principal.component.html',
  styleUrls: ['./pag-principal.component.css']
})
export class PagPrincipalComponent implements OnInit {
  public correo:string;
  public reportes:string;
  
  public lista_usuarios:Usuario [];
  public logueado: Usuario; 
  public lista_nombres_paises: string [];
  public lista_postales_del_sobre: string [];

  constructor(route:ActivatedRoute) { 
    route.params.subscribe(params=>{
      this.correo = params['correo'];
    });
    this.lista_usuarios = JSON.parse(localStorage.getItem('usuario'));
    this.obtener_usuario();
    this.lista_nombres_paises = [];
    this.lista_postales_del_sobre = [];
    this.reportes = "";
    this.llenar_lista_nombres_paises();
  }

  ngOnInit() {
  }

  obtener_usuario(){
    for (let i = 0; i < this.lista_usuarios.length; i++) {
      if(this.lista_usuarios[i].correo == this.correo){
        this.logueado = this.lista_usuarios[i];
      }
    }
  }
  abrir_sobre(){
    this.logueado.cant_total_postales += 5;
    this.lista_postales_del_sobre = [];
    for (let index = 0; index < 5; index++) {
    let ruta: string;
    ruta = "assets/selecciones/";
    let seleccion_random: number;
    seleccion_random =  Math.floor((Math.random() * 32) + 0);
    let jugador_random: number;
    jugador_random =  Math.floor((Math.random() * 20) + 1);
      for (let i = 0; i < this.logueado.lista_paises.length; i++) {
        if(this.logueado.lista_paises[i].nombre == this.lista_nombres_paises[seleccion_random]){
          ruta += this.logueado.lista_paises[i].nombre + "/";
          for (let j = 0; j < this.logueado.lista_paises[i].lista_postales.length; j++) {
            if(this.logueado.lista_paises[i].lista_postales[j].id == jugador_random){
              if(this.logueado.lista_paises[i].lista_postales[j].en_posesion == false){
                this.logueado.cant_postales_unicas += 1;
              }
              else{
                this.logueado.cant_postales_repetidas += 1;
              }
              this.logueado.lista_paises[i].lista_postales[j].en_posesion = true;
              localStorage.setItem('usuario', JSON.stringify(this.lista_usuarios));
              ruta += this.logueado.lista_paises[i].lista_postales[j].ruta;
              this.lista_postales_del_sobre.push(ruta);
              ruta = "";
            }
          }
        }
      }
    } 
  }

  obtener_cantidad_total_potales():number{
    return this.logueado.cant_total_postales;
  }
  obtener_cantidad_potales_unicas(): number{
    return this.logueado.cant_postales_unicas;
  }
  obtener_cantidad_potales_repetidas():number{
    return this.logueado.cant_postales_repetidas;
  }
  obtener_porcentaje_album_completo():number{
    let cont_postales: number;
    cont_postales = 0;
    for (let i = 0; i < this.logueado.lista_paises.length; i++) {
      for (let j = 0; j < this.logueado.lista_paises[i].lista_postales.length; j++) {
        if(this.logueado.lista_paises[i].lista_postales[j].en_posesion == true){
          cont_postales += 1;
        }
      }
    }
    return  cont_postales * 100 / 640;
  }
  obtener_cantidad_equipos_completos():number{
    let cont_equipos:number;
    cont_equipos = 0;
    for (let i = 0; i < this.logueado.lista_paises.length; i++) {
      let cont_postales: number;
      cont_postales = 0;
      for (let j = 0; j < this.logueado.lista_paises[i].lista_postales.length; j++) {
        if(this.logueado.lista_paises[i].lista_postales[j].en_posesion == true){
          cont_postales += 1;
          if(cont_postales == 20){
            cont_equipos += 1;
          }
        }
      }
    }
    return cont_equipos;
  }

  llenar_lista_nombres_paises(){
    //Grupo A
    this.lista_nombres_paises.push("russia");
    this.lista_nombres_paises.push("arabia_saudita");
    this.lista_nombres_paises.push("egypto");
    this.lista_nombres_paises.push("uruguay");
    //Grupo B
    this.lista_nombres_paises.push("portugal");
    this.lista_nombres_paises.push("espana");
    this.lista_nombres_paises.push("marruecos");
    this.lista_nombres_paises.push("iran");
    //Grupo C
    this.lista_nombres_paises.push("francia");
    this.lista_nombres_paises.push("australia");
    this.lista_nombres_paises.push("peru");
    this.lista_nombres_paises.push("dinamarca");
    //Grupo D
    this.lista_nombres_paises.push("argentina");
    this.lista_nombres_paises.push("islandia");
    this.lista_nombres_paises.push("croacia");
    this.lista_nombres_paises.push("nigeria");
    //Grupo E
    this.lista_nombres_paises.push("brasil");
    this.lista_nombres_paises.push("suiza");
    this.lista_nombres_paises.push("costa_rica");
    this.lista_nombres_paises.push("serbia");
    //Grupo F
    this.lista_nombres_paises.push("alemania");
    this.lista_nombres_paises.push("mexico");
    this.lista_nombres_paises.push("suecia");
    this.lista_nombres_paises.push("sur_korea");
    //Grupo G
    this.lista_nombres_paises.push("belgica");
    this.lista_nombres_paises.push("panama");
    this.lista_nombres_paises.push("tunes");
    this.lista_nombres_paises.push("inglaterra");
    //Grupo H
    this.lista_nombres_paises.push("polonia");
    this.lista_nombres_paises.push("senegal");
    this.lista_nombres_paises.push("colombia");
    this.lista_nombres_paises.push("japon");
  }
  
}
