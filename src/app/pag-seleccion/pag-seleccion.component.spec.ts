import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagSeleccionComponent } from './pag-seleccion.component';

describe('PagSeleccionComponent', () => {
  let component: PagSeleccionComponent;
  let fixture: ComponentFixture<PagSeleccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagSeleccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagSeleccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
