import { Component, OnInit } from '@angular/core';
import { Pagina_pais } from '../pag_pais';
import { debug } from 'util';
import { Postal } from '../postal';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-pag-seleccion',
  templateUrl: './pag-seleccion.component.html',
  styleUrls: ['./pag-seleccion.component.css']
})
export class PagSeleccionComponent implements OnInit {
  
  public lista_iconos_paises_arriba = [];
  public lista_iconos_paises_abajo = [];
  public pais_seleccionado: string;
  
  public correo:string;
  public logueado: Usuario; 
  public lista_usuarios:Usuario [];
  public porcentaje_completo_pagina: number;

  public rango_parte_arriba: number;
  public rango_parte_izquierda : number;  
  public height: number;
  public width: number;
	public lista_rangos_pagina : Array<HTMLElement> = [];

  constructor(route:ActivatedRoute) { 
    route.params.subscribe(params=>{
      this.correo = params['correo'];
    });
    this.lista_usuarios = JSON.parse(localStorage.getItem('usuario'));
    this.obtener_usuario();

    this.lista_iconos_paises_abajo = [];
    this.lista_iconos_paises_arriba = [];
    this.pais_seleccionado = "costa_rica";
    this.llenar_lista_iconos_paises_arriba();
    this.llenar_lista_iconos_paises_abajo();

    this.rango_parte_arriba = 17;
    this.rango_parte_izquierda = 3;
    this.height = 26;
    this.width = 11.5;

    this.porcentaje_pagina();
  }

  ngOnInit() {

  }

  obtener_usuario(){
    for (let i = 0; i < this.lista_usuarios.length; i++) {
      if(this.lista_usuarios[i].correo == this.correo){
        this.logueado = this.lista_usuarios[i];
      }
    }
  }

  mostar_imagen(ruta:string){
    let propiedades_postal = {
      'height': '100%',
      'width': '100%',
      'position': 'absolute',
      'content' : 'url("assets/selecciones/'+this.pagina_pais_seleccionado().nombre+"/"+ ruta +'")'
    }
    return propiedades_postal;
  }

  obtener_rangos_pagina(){
  	var pagina = document.getElementById('pagina');
  	let seccion_pagina : NodeList = pagina.getElementsByTagName('section');
  	if (seccion_pagina && this.lista_rangos_pagina.length == 0) {
	    for (let i = 0; i < seccion_pagina.length; i++) {
	        let node : Node = seccion_pagina[i];
	        if (node.nodeType == Node.ELEMENT_NODE) {
	            this.lista_rangos_pagina.push(node as HTMLElement);
	        }
	    }
	  }
  }

  propiedades_de_las_cartas() {
  let styles = {
    'left': this.rango_parte_izquierda +'%',
    'top': this.rango_parte_arriba +'%',
    'height': this.height + '%',
    'width': this.width + '%',
    'overflow': 'hidden',
    'display': 'block',
    'position': 'absolute'
  };
  if(this.rango_parte_arriba == 17 || this.rango_parte_arriba == 23){
    this.rangos_postales_arriba();
  }
  else if(this.rango_parte_arriba == 45){
    this.rango_postales_medio();
  }
  else if(this.rango_parte_arriba == 73){
    this.rango_postales_abajo();
  }
  return styles;
  }
  rangos_postales_arriba(){
    if(this.rango_parte_izquierda == 3){    
      this.rango_parte_izquierda += 12.1;
      this.rango_parte_arriba += 6;
      this.height = 20;
      this.width = 15;
    }
    else if(this.rango_parte_izquierda == 15.1  && this.rango_parte_arriba == 23){
      this.rango_parte_arriba = 17;
      this.rango_parte_izquierda += 39;
      this.height = 26;
      this.width = 11.5;
    }
    else if(this.rango_parte_izquierda == 54.1){
       this.rango_parte_izquierda += 13;
    }
    else if(this.rango_parte_izquierda == 67.1){
      this.rango_parte_izquierda = 3;
      this.rango_parte_arriba = 45;
    }
  }
  rango_postales_medio(){
    if(this.rango_parte_izquierda == 3){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 15){
    this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 27){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 39){
      this.rango_parte_izquierda += 16;
    }
    else if(this.rango_parte_izquierda == 55){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 67){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 79){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 91){
      this.rango_parte_izquierda = 3;
      this.rango_parte_arriba = 73;
    }
  }
  rango_postales_abajo(){
    if(this.rango_parte_izquierda == 3){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 15){
    this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 27){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 39){
      this.rango_parte_izquierda += 16;
    }
    else if(this.rango_parte_izquierda == 55){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 67){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 79){
      this.rango_parte_izquierda += 12;
    }
    else if(this.rango_parte_izquierda == 91){
      this.rango_parte_izquierda = 3;
      this.rango_parte_arriba = 17;
    }
  }
  llenar_lista_iconos_paises_arriba(){
    //Grupo A
    this.lista_iconos_paises_arriba.push("russia");
    this.lista_iconos_paises_arriba.push("arabia_saudita");
    this.lista_iconos_paises_arriba.push("egypto");
    this.lista_iconos_paises_arriba.push("uruguay");
    //Grupo B
    this.lista_iconos_paises_arriba.push("portugal");
    this.lista_iconos_paises_arriba.push("espana");
    this.lista_iconos_paises_arriba.push("marruecos");
    this.lista_iconos_paises_arriba.push("iran");
    //Grupo C
    this.lista_iconos_paises_arriba.push("francia");
    this.lista_iconos_paises_arriba.push("australia");
    this.lista_iconos_paises_arriba.push("peru");
    this.lista_iconos_paises_arriba.push("dinamarca");
    //Grupo D
    this.lista_iconos_paises_arriba.push("argentina");
    this.lista_iconos_paises_arriba.push("islandia");
    this.lista_iconos_paises_arriba.push("croacia");
    this.lista_iconos_paises_arriba.push("nigeria");
  }

  llenar_lista_iconos_paises_abajo(){
    //Grupo E
    this.lista_iconos_paises_abajo.push("brasil");
    this.lista_iconos_paises_abajo.push("suiza");
    this.lista_iconos_paises_abajo.push("costa_rica");
    this.lista_iconos_paises_abajo.push("serbia");
    //Grupo F
    this.lista_iconos_paises_abajo.push("alemania");
    this.lista_iconos_paises_abajo.push("mexico");
    this.lista_iconos_paises_abajo.push("suecia");
    this.lista_iconos_paises_abajo.push("sur_korea");
    //Grupo G
    this.lista_iconos_paises_abajo.push("belgica");
    this.lista_iconos_paises_abajo.push("panama");
    this.lista_iconos_paises_abajo.push("tunes");
    this.lista_iconos_paises_abajo.push("inglaterra");
    //Grupo H
    this.lista_iconos_paises_abajo.push("polonia");
    this.lista_iconos_paises_abajo.push("senegal");
    this.lista_iconos_paises_abajo.push("colombia");
    this.lista_iconos_paises_abajo.push("japon");
  }
  pais_seleccionado_barra_arriba(id:string){
    for (let i = 0; i < this.lista_iconos_paises_arriba.length; i++) {
      if(this.lista_iconos_paises_arriba[i] == id){
        this.pais_seleccionado = id;
      }
    }
  }
  pais_seleccionado_barra_abajo(id:string){
    for (let i = 0; i < this.lista_iconos_paises_abajo.length; i++) {
      if(this.lista_iconos_paises_abajo[i] == id){
        this.pais_seleccionado = id;
      }
    }
  }
  pagina_pais_seleccionado():Pagina_pais{
    for (let i = 0; i < this.logueado.lista_paises.length; i++) {
      if(this.logueado.lista_paises[i].nombre == this.pais_seleccionado){
        return this.logueado.lista_paises[i];
      }
    }
  }
  porcentaje_pagina(): number{ 
    let porcentaje: number;   
    porcentaje = 0;
    this.porcentaje_completo_pagina= 0;
    for (let i = 0; i < this.logueado.lista_paises.length; i++) {
      if(this.logueado.lista_paises[i].nombre == this.pais_seleccionado){
        for (let j = 0; j < this.logueado.lista_paises[i].lista_postales.length; j++) {
          if(this.logueado.lista_paises[i].lista_postales[j].en_posesion == true)  {
            porcentaje += 1;
            this.porcentaje_completo_pagina = porcentaje / 20 * 100;
            this.logueado.lista_paises[i].porcenteje_lleno = this.porcentaje_completo_pagina;
            localStorage.setItem('usuario', JSON.stringify(this.lista_usuarios));
            this.logueado.lista_paises[i].porcenteje_lleno = this.porcentaje_completo_pagina;
          }  
        }
      }
    }
    
    return this.porcentaje_completo_pagina;
  }
  
}